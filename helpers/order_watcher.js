const telegrambot = require('./telegrambot.js');
const tbot = new telegrambot(global.config.TELEGRAMBOT);


class OrderWatcher {
    constructor(apiId) {
        this.apiId = apiId;
        this.start();
    }
    async start() {
        if (this.interval) {
            console.error("order watcher interval already running!");
            return;
        }
        try {
            const api = await global.db.getApi(this.apiId);
            if (!api) {
                console.error("invalid apiId: " + this.apiId);
                return;
            }
            this.exchanger = new global.exchanges_class[api.market](JSON.parse(api.data));
            this.userData = await global.db.getUserById(api.user);
            if(!this.userData){
                console.error("invalid user for apiId: " + this.apiId);
                return;
            }
            this.currentOrders = api.orders ? JSON.parse(api.orders) : {};
        } catch (error) {
            console.error("error on starting orders watcher for " + this.apiId + ": ", error);
        }
        const self = this;
        this.interval = setInterval(async () => {
            try {
                const orders = await self.exchanger.margin_open_orders();
                // console.log("orders for api "+self.apiId+": ",orders);
                await self.compare_orders('margin', orders);
            } catch (e) {
                console.error("error on order watcher interval: ", e);
            }
        }, 60 * 1000);
    }
    async compare_orders(type, orders) {
        const current = this.currentOrders[type];
        if (!current) {
            this.currentOrders[type] = {};
            for (var i in orders) {
                this.currentOrders[type][orders[i].id] = orders[i];
            }
            console.log("order watcher current orders are set: ", orders);
            this.save_orders();
            return;
        }
        // console.log("comparing orders: ",current,orders);
        const notify = [], activeIds = [];
        const orderText = function (o) {
            return (o.buy ? "➕":"➖") + o.amount + " " + o.symbol + " @ " + o.price;
        }
        for (var i in orders) {
            const o = orders[i];
            activeIds.push(o.id);
            if (!(o.id in current)) {
                //new order
                notify.push("💎 New Order: " + orderText(o));
            } else if (current[o.id].filled != o.filled) {
                //partially filled
                notify.push("☑️ Order " + orderText(o) + " Partially Filled to: " + o.filled);
            } else {
                continue;
            }
            current[o.id] = o;
        }
        for (var i in current) {
            if (activeIds.indexOf(i) < 0) {
                const o=await this.exchanger.margin_get_order(current[i].symbol,current[i].id);
                // console.log("order not found: ",o);
                notify.push(
                    (o.status=='CANCELED'?"🗑Order Cancelled: ":"✅✅ Order Completed: ") 
                    + orderText(current[i]));
                delete current[i];
            }
        }
        if (notify.length > 0) {
            console.log("order watcher for " + this.apiId + " new notifications: ", notify);
            await this.save_orders();
            await tbot.sendMessage(this.userData.telegramid, notify.join("\n\n"));
        }
    }
    async save_orders() {
        await global.db.updateApi(this.apiId, { orders: JSON.stringify(this.currentOrders) });
    }
}

module.exports = OrderWatcher;
