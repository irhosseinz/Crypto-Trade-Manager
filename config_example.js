module.exports.APIS={
	binance:{
		name:'Binance'
		,keys:['key','secret']
	}
};

//telegram bot for use in sending notifications to users
module.exports.TELEGRAMBOT_USERNAME='';//telegram bot username 
module.exports.TELEGRAMBOT='';//bot token

//module.exports.db={
//	type:'mysql'
//	,data:{
//		host     : 'localhost',
//		user     : 'me',
//		password : 'secret',
//		database : 'my_db'
//		}
//};
module.exports.db={
	type:'sqlite'
	,data:{
		database : 'DB'
		}
};
module.exports.https=false;


module.exports.title='Trade Manager';


module.exports.recaptcha_v3={
	active:false//you can activate or deactivate recaptcha here
	,key:'key'
	,secret:'secret'
};

module.exports.cookie_life=900000;

module.exports.port=1040;

module.exports.cycle_second=10;
module.exports.cycle_second=10;//run every second
module.exports.wait_after_jump=30;//wait after prices levels changes then decide

