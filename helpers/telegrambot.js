// telegram-bot-manager.js

const axios = require('axios');
const fs = require('fs');
var FormData = require('form-data');

class TelegramBotManager {
    constructor(token) {
        this.token = token;
        this.baseURL = `https://api.telegram.org/bot${token}`;
        this.lastFetchedUpdate = 0;
    }

    async apiCall(method, data) {
        try {
            const response = await axios.post(`${this.baseURL}/${method}`, data);
            return response.data;
        } catch (error) {
            if (!error.response) {
                console.error('Error on telegram api call ' + method + ':', (error && error.code) ? error.code : error);
                return {};
            }
            console.error('Error from telegram api call ' + method + ':', error.response.data);
            return error.response.data;
        }
    }
    async setWebhook(url) {
        return await this.apiCall('setWebhook', { url });
    }
    async getWebhook() {
        return await this.apiCall('getWebhookInfo', {});
    }
    async getUpdates(offset) {
        return await this.apiCall('getUpdates', { offset });
    }
    async listenForUpdates(processUpdates) {
        if (this.getUpdatesInterval) {
            console.error("listening for telegram bot updates already running!");
            return;
        }
        const self = this;
        this.getWebhook().then(resp => {
            if (!resp.ok || resp.result.url) {
                console.error("telegram bot has a webhook set. unable to read updates");
                return;
            }
            self.getUpdatesInterval = setInterval(async () => {
                try {
                    const updates = await self.getUpdates(self.lastFetchedUpdate);
                    if (updates.ok) {
                        // console.log("updates from "+self.lastFetchedUpdate+": ", updates.result);
                        for (var i in updates.result) {
                            self.lastFetchedUpdate = Math.max(self.lastFetchedUpdate, updates.result[i].update_id+1);
                        }
                        processUpdates(updates.result);
                    }
                } catch (e) {

                }
            }, 5000);
            console.log("listening for telegram bot updates.. ");
        });
    }
    async answerCallbackQuery(callback_query_id, text, alert) {
        const data = {
            callback_query_id,
        };
        if (text) {
            data.text = text;
            if (alert) {
                data.show_alert = true;
            }
        }
        return await this.apiCall('answerCallbackQuery', data);
    }
    async editMessageReplyMarkup(chat_id, message_id, reply_markup) {
        return await this.apiCall('editMessageReplyMarkup', {
            chat_id,
            message_id,
            reply_markup,
        });
    }
    async editMessage(chat_id, message_id, text, reply_markup) {
        return await this.apiCall('editMessageText', {
            chat_id,
            message_id,
            text,
            reply_markup,
        });
    }

    async sendPhoto(chatId, file_id, caption, keyboard, parse_mode) {
        if (chatId < 0) {
            return;
        }
        const msg = {
            chat_id: chatId,
            caption: caption,
            photo: file_id,
        };
        if (parse_mode) {
            msg.parse_mode = parse_mode;
        }
        if (keyboard) {
            msg.reply_markup = keyboard;
        }
        if (msg.caption && msg.caption.length > 1000) {
            msg.caption = msg.caption.substring(0, 1000) + "\n...";
        }
        return await this.apiCall('sendPhoto', msg);
    }
    async getDownloadLink(file_id) {
        try {
            const response = await this.apiCall('getFile', { file_id });
            if (!response || !response.ok) {
                throw new Error("Error on connecting for file link");
            }
            if (!response.data) {
                throw new Error("Invalid Data: " + JSON.stringify(response));
            }
            return 'https://api.telegram.org/file/bot' + this.token + '/' + response.data.result.file_path;
        } catch (error) {
            if (!error.response) {
                console.error('Error getting file link:', error);
                return;
            }
            console.error('Error getting file link:', error.response.data);
        }
    }
    async pipeTelegramFile(file_id, res) {
        try {
            const link = await this.getDownloadLink(file_id);
            if (!link) {
                throw new Error("no link to download");
            }
            const response = await axios({
                method: 'get',
                url: link,
                responseType: 'stream'
            });

            // Set the appropriate headers for the response
            if (response.headers['content-type'])
                res.setHeader('Content-Type', response.headers['content-type']);
            if (response.headers['content-length'])
                res.setHeader('Content-Length', response.headers['content-length']);

            // Pipe the photo stream to the response
            response.data.pipe(res);
        } catch (error) {
            res.sendStatus(404);
        }
    }
    async sendPhotoFile(chatId, photoPath, caption, keyboard, parse_mode) {
        if (chatId < 0) {
            return;
        }
        try {
            if (!fs.existsSync(photoPath)) {
                console.error("file " + photoPath + " does not exists");
                return {};
            }
            const photoStream = fs.createReadStream(photoPath);
            const formData = new FormData();
            formData.append('chat_id', chatId);
            formData.append('photo', photoStream);

            if (caption) {
                formData.append('caption', caption);
            }

            if (parse_mode) {
                formData.append('parse_mode', parse_mode);
            }

            if (keyboard) {
                formData.append('reply_markup', JSON.stringify(keyboard));
            }

            const response = await axios.post(`${this.baseURL}/sendPhoto`, formData, {
                headers: formData.getHeaders(),
            });

            return response.data;
        } catch (error) {
            if (!error.response) {
                console.error('Error sending photo file:', error);
                return {};
            }
            console.error('Error sending photo file:', error.response.data, { chatId, photoPath, caption, parse_mode });
            return error.response.data;
        }
    }
    async sendMessage(chatId, text, markup, parse_mode) {
        if (chatId < 0) {
            return;
        }
        const msg = {
            chat_id: chatId,
            text,
        };
        if (markup) {
            msg.reply_markup = {
                resize_keyboard: true,
                keyboard: markup
            };
        }
        if (parse_mode) {
            msg.parse_mode = parse_mode;
        }
        return await this.sendMessageRaw(msg);
    }
    async sendMessageRaw(message) {
        if (message.chat_id < 0) {
            return;
        }
        if (message.text && message.text.length > 4000) {
            message.text = message.text.substring(0, 4000) + "\n...";
        }
        return await this.apiCall('sendMessage', message);
    }
    async answerCheckout(id, error) {
        const data = {
            pre_checkout_query_id: id,
            ok: error ? false : true,
        };
        if (error) {
            data.error_message = error;
        }
        return await this.apiCall('answerPreCheckoutQuery', data);
    }
    async sendInvoice(data) {
        return await this.apiCall('sendInvoice', data);
    }
    async getStarTransactions(offset) {
        try {
            const resp = await this.apiCall('getStarTransactions', { offset: offset ? offset : 0 });
            if (!resp || !resp.ok) {
                throw new Error(resp ? resp.error : resp);
            }
            return resp.result.transactions;
        } catch (error) {
            console.error('Error getStarTransactions:', error);
            return null;
        }
    }
    async profilePhotos(user_id) {
        return await this.apiCall('getUserProfilePhotos', { user_id });
    }
    async sendSticker(chat_id, sticker) {
        return await this.apiCall('sendSticker', { chat_id, sticker });
    }
    async sendVideo(chat_id, video) {
        return await this.apiCall('sendVideo', { chat_id, video });
    }
    async downloadFile(file_id, filePath) {
        if (!file_id) {
            return;
        }
        const self = this;
        return new Promise((resolve, reject) => {
            self.getDownloadLink(file_id).then(fileUrl => {
                if (!fileUrl) {
                    reject("error on getting file data");
                }
                return axios({
                    url: fileUrl,
                    method: 'GET',
                    responseType: 'stream'
                });
            }).then(fileResponse => {
                // Create a writable stream and pipe the file response to it
                const writer = fs.createWriteStream(filePath);
                fileResponse.data.pipe(writer);

                writer.on('finish', resolve);
                writer.on('error', reject);
            });
        });
    }
}

module.exports = TelegramBotManager;
